/**
 * @file test06.cpp
 * @author toge (toge.mail@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2020-09-24
 * 
 * @copyright Copyright (c) 2020
 * https://github.com/arun11299/cpp-jwt#additional-header-data
 */

#include <iostream>
#include <cassert>

#include <jwt/jwt.hpp>

auto main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) -> int {
    std::cout << std::boolalpha;
    std::cin.tie(nullptr);
    std::ios_base::sync_with_stdio(false);
    
    auto obj = jwt::jwt_object{
        jwt::params::headers({
            {"alg", "none"},
            {"typ", "jwt"},
        }),
        jwt::params::payload({
            {"iss", "arun.muralidharan"},
            {"sub", "nsfw"},
            {"x-pld", "not my ex"}
        })
    };

    auto ret = obj.header().add_header("kid", 1234567);
    assert(ret);

    ret = obj.header().add_header("crit", std::array<std::string, 1>{"exp"});
    assert(ret);

    auto ec = std::error_code{};
    auto enc_str = obj.signature();

    auto dec_obj = jwt::decode(enc_str, jwt::params::algorithms({"none"}), ec, jwt::params::verify(true));

    assert(ec.value() == static_cast<int>(jwt::AlgorithmErrc::NoneAlgorithmUsed));

    return 0;
}