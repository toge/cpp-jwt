/**
 * @file test02.cpp
 * @author toge (toge.mail@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2020-09-24
 * 
 * @copyright Copyright (c) 2020
 * https://github.com/arun11299/cpp-jwt#example
 */

#include <iostream>
#include <chrono>
#include <cassert>

#include <jwt/jwt.hpp>

auto main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) -> int {
    std::cout << std::boolalpha;
    std::cin.tie(nullptr);
    std::ios_base::sync_with_stdio(false);
    
    auto obj = jwt::jwt_object{jwt::params::algorithm("HS256"), jwt::params::secret("secret"), jwt::params::payload({{"user", "admin"}})};
    obj
        .add_claim("iss", "arun.muralidharan")
        .add_claim("sub", "test")
        .add_claim("id", "a-b-c-d-e-f-1-2-3")
        .add_claim("iat", 15138623171)
        .add_claim("exp", std::chrono::system_clock::now() + std::chrono::seconds(10))
        ;

    assert(obj.payload().has_claim(jwt::registered_claims::issuer));
    assert(obj.payload().has_claim(jwt::registered_claims::expiration));

    assert(obj.payload().has_claim_with_value("id", "a-b-c-d-e-f-1-2-3"));
    assert(obj.payload().has_claim_with_value(jwt::registered_claims::issued_at, 15138623171));

    obj.remove_claim(jwt::registered_claims::expiration);
    assert(not obj.has_claim(jwt::registered_claims::expiration));

    bool ret = obj.payload().add_claim("sub", "new test", false); // false : not overwrite
    assert(not ret);

    ret = obj.payload().add_claim("sub", "new test", true); // true : overwrite
    assert(ret);

    assert(obj.payload().has_claim_with_value("sub", "new test"));

    return 0;
}
