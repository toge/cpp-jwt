/**
 * @file test04.cpp
 * @author toge (toge.mail@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2020-09-24
 * 
 * @copyright Copyright (c) 2020
 * https://github.com/arun11299/cpp-jwt#advanced-examples
 */

#include <cassert>
#include <iostream>

#include <jwt/jwt.hpp>

auto main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) -> int {
    std::cout << std::boolalpha;
    std::cin.tie(nullptr);
    std::ios_base::sync_with_stdio(false);
    
    auto obj = jwt::jwt_object{jwt::params::algorithm("HS256"), jwt::params::secret("secret")};

    obj.add_claim(jwt::registered_claims::issuer, "arun.muralidharan");
    obj.add_claim(jwt::registered_claims::expiration, std::chrono::system_clock::now() - std::chrono::seconds{1});

    auto enc_str = obj.signature();

    try {
        auto dec_obj = jwt::decode(enc_str, jwt::params::algorithms({"HS256"}), jwt::params::secret("secret"), jwt::params::verify(true));
    } catch(const jwt::TokenExpiredError& ex) {
        std::cerr << "token expired : " << ex.what() << std::endl;
    } catch(const jwt::SignatureFormatError& ex) {
        std::cerr << "invalid signature : " << ex.what() << std::endl;
    } catch(const jwt::DecodeError& ex) {
        std::cerr << "decode error : " << ex.what() << std::endl;
    } catch(const jwt::VerificationError& ex) {
        std::cerr << "verification : " << ex.what() << std::endl;
    } catch (...) {
        std::cerr << "unknown exception " << std::endl;
    }

    return 0;
}