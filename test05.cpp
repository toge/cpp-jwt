/**
 * @file test05.cpp
 * @author toge (toge.mail@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2020-09-24
 * 
 * @copyright Copyright (c) 2020
 * https://github.com/arun11299/cpp-jwt#advanced-examples
 */

#include <cassert>
#include <iostream>

#include <jwt/jwt.hpp>

auto main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) -> int {
    std::cout << std::boolalpha;
    std::cin.tie(nullptr);
    std::ios_base::sync_with_stdio(false);

    auto obj = jwt::jwt_object{jwt::params::algorithm("HS256"), jwt::params::secret("secret"), jwt::params::payload({{"sub", "test"}})};

    auto ec = std::error_code {};
    auto enc_str = obj.signature(ec);
    assert(not ec);

    auto dec_obj = jwt::decode(enc_str, jwt::params::algorithms({"HS256"}), ec, jwt::params::secret("secret"), jwt::params::issuer("arun.muralidharan"));
    assert(ec);

    assert(ec.value() == static_cast<int>(jwt::VerificationErrc::InvalidIssuer));    

    return 0;
}