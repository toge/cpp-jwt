/**
 * @file test01.cpp
 * @author toge (toge.mail@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2020-09-24
 * 
 * @copyright Copyright (c) 2020
 * https://github.com/arun11299/cpp-jwt#example
 */

#include <iostream>
#include "jwt/jwt.hpp"

auto main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) -> int {
    std::cout << std::boolalpha;
    std::cin.tie(nullptr);
    std::ios_base::sync_with_stdio(false);
    
    auto  key = "secret";
    auto       obj = jwt::jwt_object{
                        jwt::params::algorithm("HS256"), 
                        jwt::params::payload({{"some", "payload"}}), 
                        jwt::params::secret(key)
                    };

    auto ec = std::error_code {};
    
    auto enc_str = obj.signature(ec);
    if (ec) {
        std::cerr << "failed to signature" << std::endl;
        return 1;
    }
    std::cout << enc_str << std::endl;

    auto dec_obj = jwt::decode(enc_str, jwt::params::algorithms({"HS256"}), ec, jwt::params::secret(key));
    if (ec) {
        std::cerr << "failed to decode " << std::endl;
        return 2;
    }

    std::cout << "----- header  ----" << std::endl;
    std::cout << dec_obj.header() << std::endl;
    std::cout << "----- payload ----" << std::endl;
    std::cout << dec_obj.payload() << std::endl;

    return 0;
}